public class CountingUpAndDown{
    public static void main(String[] args){
        int count = 0;
        System.out.println("Counting up:");
        for (int i=0;i<7;i++){
            count++;
            System.out.println(count);
        }
        System.out.println("Counting down:");
        while (count > 0){
            count--;
            System.out.println(count);
        }
    }
}
