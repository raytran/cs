public class MultipleCounterTester{
    public static void main(String[] args){
        MultipleCounter mCounters[] = {
            new MultipleCounter(5,0,35),
            new MultipleCounter(3,2,12),
            new MultipleCounter(4,-6,20)
        };
        for (MultipleCounter mc : mCounters){
            System.out.println("Counting from " + mc.getStartVal() +" to "+ mc.getEndVal()
                    +" by " + mc.getCountBy() + "'s");
            mc.showCounting();
        }
    }
}
