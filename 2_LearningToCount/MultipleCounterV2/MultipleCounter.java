public class MultipleCounter{
    private int countBy;
    private int startVal;
    private int endVal;

    MultipleCounter(int countBy,int startVal,int endVal){
        if (endVal > startVal && countBy < 0) throw new Error("You can't count up with a negative number");
        else if (endVal < startVal && countBy > 0) throw new Error("You can't count down with a positive number");
        else if ((endVal - startVal) % countBy != 0) throw new Error("Not a countable multiple");
        this.countBy = countBy;
        this.startVal = startVal;
        this.endVal = endVal;
    }

    public void showCounting(){
        int count = startVal;
        while (count != endVal + countBy){
            System.out.println(count);
            count += countBy;
        }
    }

    public int getCountBy(){
        return countBy;
    }

    public int getStartVal(){
        return startVal;
    }

    public int getEndVal(){
        return endVal;
    }
}

    

