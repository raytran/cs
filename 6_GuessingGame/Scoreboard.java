/**
 * Scoreboard creates a scoreboard that displays player scores
 * Fields: players
 * Public Methods: fancyToString, setPlayers, 
 * @author: Raymond Tran
 * @version: 1.0
 * @date: 9/25/18
 */
import java.util.Arrays;
import java.util.Comparator;
public class Scoreboard{
    private Player[] players;
    
    public Scoreboard(Player[] players){
	this.players = players;
    }

    public String toString(){
	return "Scoreboard with " + players.length + " players";
    }
    
    public String fancyToString(){
	//Print a thing with the scores
	String returnString = "";
	//find the max player name
	int nameLength = 0;
	for (int i=0;i<players.length;i++){
	    int currentNameLength = players[i].getName().length();
	    if (currentNameLength > nameLength) nameLength = currentNameLength;
	}
	if (nameLength < 4) nameLength = 4;
	//Format heading
	String playerNameHeading = "Name";
	playerNameHeading = appendStringPadding(playerNameHeading,nameLength);

	returnString += "Rank # |" + playerNameHeading + " | W | L | Win % |";
	int breakLength = returnString.length();
	returnString += "\n";
	for (int i=0;i<breakLength;i++){
	    returnString += "-";
	}
	returnString += "\n";
	
	//PRINT THE RANKED PLAYERS
	Player[] sortedPlayers = rankPlayers();
	for (int i=0;i<sortedPlayers.length;i++){
	    returnString += appendStringPadding("" + (i+1),7);
	    returnString += "|";
	    returnString += getPlayerRow(sortedPlayers[i],nameLength);
	    returnString += "\n";
	}


	

	return returnString;
    }

    public Player[] getPlayers(){return players;}
    public void setPlayers(Player[] p){
	players = p;
    }

    //Return an array of players sorted by rank
    //player[0] = rank 1
    private Player[] rankPlayers(){
	Player[] sortedArr = new Player[players.length];
	for (int i=0;i<players.length;i++){
	    sortedArr[i] = players[i];
	}

	Arrays.sort(sortedArr,new Comparator<Player>(){
		@Override
		public int compare(Player a,Player b){
		    return b.getWins() - a.getWins();
		}
	    });

	return sortedArr;
    }
    
    //Returns "name | W | L | % |
    private String getPlayerRow(Player currentPlayer,int maxNameLength){
	String returnString = "";
	String name = currentPlayer.getName();
	for (int j=name.length();j<maxNameLength;j++){
	    name += " ";
	}
	returnString += name + " | ";
	returnString += currentPlayer.getWins() + " | ";
	returnString += currentPlayer.getLosses() + " | ";
	returnString += getWinPercentage(currentPlayer) + "%";
	return returnString;
    }

    //Returns a player's win percentage as an integer
    private int getWinPercentage(Player player){
	return (int)(100.0 * (player.getWins()/(double)(player.getWins() + player.getLosses())));
    }

    //Appends blank spaces to a string until it reaches a desired length
    private String appendStringPadding(String string,int targetStrLength){
	String returnString = string;
	for (int i=string.length();i<targetStrLength;i++){
	    returnString += " ";
	}
	return returnString;

    }

}
