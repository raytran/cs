import java.util.*;
class Raymond extends Person implements IStudent, IProgrammer{
    private List<Assignment> workDue = new ArrayList<Assignment>();
    private List<Assignment> workDone = new ArrayList<Assignment>();
    Raymond(){
        this.heightCM = 4;
        this.weightKG = 4;
        this.name = "Raymond";
    }
    
    public List<Assignment> getCompletedAssignments(){
        return workDone;
    }
    public void completeAssignments(){
        raySay("Doing assignments...");
        for (Assignment assignment : workDue){
            workDone.add(assignment);
            assignment.setCompleted(true);
        }

        for (Assignment assignment : workDone){
            workDue.remove(assignment);
        }
        raySay("Done!");
 
    }
    public void addAssignment(Assignment assignment){
        workDue.add(assignment);
    }
    public void program(){};

    public void study(){
        raySay("Hold on let me study physics first");
        raySay("Okay ready to go!");
    //TODO ........
    }
    private void raySay(String msg){
        System.out.println("Raymond Says: " + msg);
    
    }


}
