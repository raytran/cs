public class Assignment{
    private String description;
    private int pointsPossible;
    private int pointsAchieved;
    private boolean completed;

    Assignment(String desc,int pointsPos){
        description = desc;
        pointsPossible = pointsPos;
        completed = false;
    }

    public void setCompleted(boolean b){
        this.completed = b;
    }
    public boolean isCompleted(){
        return completed;
    }
    public String getDescription(){
        return description;
    }
    public void setPointsAchieved(int p){
        this.pointsAchieved = p;
    }
    public int getPointsPossible(){
        return pointsPossible;
    }
    public int getPointsAchieved(){
        return pointsAchieved;
    }
    



}
