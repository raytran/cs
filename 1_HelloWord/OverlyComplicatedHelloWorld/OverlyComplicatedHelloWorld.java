/**
 * This program simulates the interaction between Mr. Carr and Raymond
 *
 * Mr. Carr assigns a new HelloWorld programming assignment, which 
 * Raymond completes after his physics homework.
 * (I don't know why I did this either)
 * @author: Raymond Tran
 * @version: 1.0
 * @date: 08/17/18
 *
 */
import java.util.*;
public class OverlyComplicatedHelloWorld{
    public static void main(String[] args){
        MrCarr mrCarr = new MrCarr();
        Raymond raymond = new Raymond();
        
        // /THIS/ assignment aha
        Assignment helloWorldAssignment = new ProgrammingAssignment("Hello World Assignment",100);
        mrCarr.assign(helloWorldAssignment);
        mrCarr.printAssignmentList();

        //Add to assignment list
        raymond.addAssignment(helloWorldAssignment);
        //Do some physics first
        raymond.study();
        //Do helloworld assignment
        raymond.completeAssignments();

        //Grading
        List<Assignment> raymondsAssignments = raymond.getCompletedAssignments();
        for (Assignment as : raymondsAssignments){
            mrCarr.grade(as);
        }
    }
}
