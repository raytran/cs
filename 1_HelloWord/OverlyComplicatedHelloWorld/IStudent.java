interface IStudent{
    void addAssignment(Assignment assignment);
    void completeAssignments();
    void study();

}
