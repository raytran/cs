import java.util.*;
class MrCarr extends Person implements ITeacher, IProgrammer{
    private List<Assignment> assignmentList = new ArrayList<Assignment>();
    MrCarr(){
        this.heightCM = 90000000;
        this.weightKG = 90000000;
        this.name = "Mr. Carr";
    }

    public void printAssignmentList(){
        carrTalk("Current Assignments: ");
        for (Assignment assignment : assignmentList){
            System.out.println(assignment.getDescription());
        }
    
    }
    public void assign(Assignment assignment){
        assignmentList.add(assignment);
    }

    public void grade(Assignment assignment){
        carrTalk("Grading assignments");
        if (assignment.isCompleted()){
            if (assignment instanceof ProgrammingAssignment){
                carrTalk("I'm running the assignment. Program output: ");
                ((ProgrammingAssignment)assignment).run();
            }
            assignment.setPointsAchieved(100);
            assignmentList.remove(assignment);
            carrTalk("Good work");
        }else{
            carrTalk("Why did you turn in an empty document?");
        }
    }
    public Assignment getOldestAssignment(){
        return assignmentList.get(0);
    }

    public void program(){
        //stuff 
    }

    private void carrTalk(String words){
        System.out.println("Mr Carr Says: " + words);
    }

}
