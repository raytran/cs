interface ITeacher{
    void assign(Assignment assignment);
    void grade(Assignment assignment);
}
