/**
 * Shape is an abstract class that represents a basic shape
 * Fields: x, y, width, height
 * Methods: area, perimeter, toString, getX, getY, getWidth, getHeight
 * @author: Raymond Tran
 * @version: 1.0
 * @date: 9/10/18
 */
public abstract class Shape{
    double x;
    double y;
    double height;
    double width;
    public Shape(double x, double y, double width, double height){
        if (width < 0) throw new IllegalArgumentException("width cannot be negative");
        if (height < 0) throw new IllegalArgumentException("height cannot be negative");
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }
    public double getWidth(){return width;}
    public void setWidth(double w){width = w;}
    public double getHeight(){return height;}
    public void setHeight(double h){height = h;}
    public double getX(){return x;}
    public void setX(double x){this.x = x;}
    public double getY(){return y;}
    public void setY(double y){this.y = y;}

    public double area(){
        return height*width;
    }
    public String toString(){
        return this.getClass() + " X: " +x + " Y: " + " height: " + height + " width: " + width + " area: " + area() + " perimeter: " +perimeter();
    }
    public abstract double perimeter();
}
