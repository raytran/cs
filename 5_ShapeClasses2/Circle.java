/**
 * Circle creates a circle w/x y position defined at the center
 * Fields: x, y, r
 * Methods: area, perimeter, toString
 * @author: Raymond Tran
 * @version: 1.0
 * @date: 9/10/18
 */
public class Circle extends Shape{
    private double r;
    public Circle(double x, double y, double r){
        super(x,y,r/2,r/2);
        if (r < 0) throw new IllegalArgumentException("Radius cannot be negative");
        this.r = r;
    }

    public Circle(double x, double y, double width, double height){
        super(x,y,width,height);
        if (width < 0 || height < 0) throw new IllegalArgumentException("W/H cannot be negative");
        //Width/height are diameters, default to highest value
        this.r = (width > height ? height/2 : width/2);
    }
    public double area(){
        return Math.PI * Math.pow(r,2);
    }
    public double perimeter(){
        return 2 * Math.PI * r;
    }
}
