/**
 * RightTriangle represents a right triangle with an xy position defined by its top left corner
 * Fields: x, y, width, height
 * Methods: area, perimeter, toString
 * @author: Raymond Tran
 * @version: 1.0
 * @date: 9/10/18
 */
public class RightTriangle extends Shape{
    public RightTriangle(double x, double y, double width, double height){
        super(x,y,width,height);
    }
    public double area(){
        return super.area() / 2;
    }
    public double perimeter(){
        double hypot = Math.sqrt( Math.pow(width,2) + Math.pow(height,2) );
        return width + height + hypot;
    }
}
