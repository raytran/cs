/**
 * CalcTester is used to test the Calc class
 * @author: Raymond Tran
 * @version: 2.0
 * @date: 8/29/18
 */
public class CalcTester{
    public static void main(String[] args) {
        Calc calc1 = new Calc(2,6);
        Calc calc2 = new Calc(0,0);

        //Calculate (2+6) * (6-2)
        //calc2B.A = (2+6)
        //calc2B.B = (6-2)
        //(2+6) * (6-2) = calc2B.A * calc2B.B

        calc2.setA(calc1.getAplusB());
        calc2.setB(calc1.getBminusA());
        System.out.println("(2+6) * (6-2) is: ");
        System.out.println(calc2.getAtimesB());




    }

    private static void testCalc(Calc myCalc){
        int a = myCalc.getA();
        int b = myCalc.getB();
        System.out.println("Testing calc with #'s :" + a + " " + b);
        System.out.println(a + " plus " + b + " is " + myCalc.getAplusB());
        System.out.println(a + " times " + b + " is " + myCalc.getAtimesB());
        System.out.println(a + " minus " + b + " is " + myCalc.getAminusB());
        System.out.println(b + " minus " + a + " is " + myCalc.getBminusA());
        System.out.println(a + " divided by " + b + " is " + myCalc.getAdivB());
        System.out.println(b + " divided by " + a + " is " + myCalc.getBdivA());
    }





}
