/**
 * CalcTester is used to test the Calc class
 * @author: Raymond Tran
 * @version: 1.0
 * @date: 8/29/18
 */
public class CalcTester{
    public static void main(String[] args){
        Calc[] calculators = {
            new Calc(3,7),
            //Testing
            new Calc(-7,3),
            //new Calc(-3,7),
            //new Calc(3,-7),
            //new Calc(-7,3),
            //new Calc(7,-3),
            //new Calc(-3,-7),
            //new Calc(-7,-3)
        
        
        
        };
        for (Calc calc: calculators){
            testCalc(calc);
        }
    }



    private static void testCalc(Calc myCalc){
        int a = myCalc.getA();
        int b = myCalc.getB();
        System.out.println("Testing calc with #'s :" + a + " " + b);
        System.out.println(a + " plus " + b + " is " + myCalc.getAplusB());
        System.out.println(a + " times " + b + " is " + myCalc.getAtimesB());
        System.out.println(a + " minus " + b + " is " + myCalc.getAminusB());
        System.out.println(b + " minus " + a + " is " + myCalc.getBminusA());
        System.out.println(a + " divided by " + b + " is " + myCalc.getAdivB());
        System.out.println(b + " divided by " + a + " is " + myCalc.getBdivA());
    }




}
