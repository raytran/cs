/**
 * Calc is used to perform simple math on two integers
 * Fields: int1, int2
 * Methods: 
 *      setA, setB, getA, getB
 *      getAplusB,  getAminusB, getBminusA
 *      getAtimesB, getAdivB,   getBdivA
 * @author: Raymond Tran
 * @version: 1.0
 * @date: 8/29/18
 */
public class Calc{
    int intA;
    int intB;
    Calc(int int1,int int2){
        intA = int1;
        intB = int2;
    }

    //Public
    public void setA(int a){
        intA = a;
    }

    public void setB(int b){
        intB = b;
    }

    public int getA(){
        return intA;
    }
    public int getB(){
        return intB;
    }
    public int getAplusB(){
        return badAdd(intA,intB);
    }
    public int getAtimesB(){
        return badMulti(intA,intB);
    }
    public int getAdivB(){
        return badDiv(intA,intB);
    }
    public int getBdivA(){
        return badDiv(intB,intA);
    }
    public int getAminusB(){
        return badSubtract(intA,intB);
    }
    public int getBminusA(){
        return badSubtract(intB,intA);
    }

    //Private
    private int badAdd(int a,int b){
        int sum = a;
        for (int i=0;i<badAbs(b);i++){
            if (b<0) sum--;
            else sum++;
        }
        return sum;
    }
    private int badSubtract(int a,int b){
        int diff = a;
        for (int i=0;i<badAbs(b);i++){
            if (b<0) diff++;
            else diff--;
        }
        return diff;
    }

    private int badMulti(int a, int b){
        int product = 0;
        //Add /a/ a total of /b/ times
        for (int i=0;i<badAbs(b);i++){
            if (b>0) product = badAdd(product,a);
            else product = badSubtract(product,a);
        }
        return product;
    }
    private int badDiv(int a, int b){
        int quotient = 0;
        int aCopy = a;
        //Subtract b from a until you can't, while counting the
        //number of subtractions
        int absB = badAbs(b);
        while (badAbs(a) > absB){
            a = badSubtract(badAbs(a),absB);
            //If the quotient should result in a positive #
            if ((aCopy>0 && b >0) 
                    || (aCopy<0 && b<0)){
                quotient++;
            }
            else quotient--;
        }
        return quotient;
    }

    //Who needs badAbs() anyway
    private int badAbs(int a){
        if (a < 0){
           int b = 0;
           int returnVal = 0;
           while (b != a){
               returnVal++;
               b--;
           }
           return returnVal;
        }else{
           return a; 
        }
    }
}
