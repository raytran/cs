/**
 * MultiTable creates a 10x10 multiplication table 
 * @author: Raymond Tran
 * @date: 8/29/18
 */
public class MultiTable1{
    public static void main(String[] args){
        int xMax = 10;
        int yMax = 10;
        int maxDigitLength = String.valueOf(yMax*xMax).length();

        for (int i=1;i<=xMax;i++){
            for (int j=1;j<=yMax;j++){

                String finalPrint = String.valueOf(i * j);
                int length = String.valueOf(i*j).length();
                while (length < maxDigitLength){
                    length ++;
                    finalPrint += " ";
                }
                System.out.print(finalPrint);

            }
            System.out.println("");
        }
    }
}
