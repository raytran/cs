/**
 * MultiTable3 creates Multiplication Table strings
 * Fields: xyMax1 - x by x dimensions
 * Methods: getTableString
 * @author: Raymond Tran
 * @version: 1.0
 * @date: 8/29/18
 */
public class MultiTable3{
    int xyMax;
    public MultiTable3(int xyMax1){
        if (xyMax1 > 12) throw new Error("XY max cannot be greater than 12");
        xyMax = xyMax1;
    }
    //getTableString returns the multiplication table as a String
    public String getTableString(){
        String returnString = "";
        int xMax = xyMax;
        int yMax = xyMax;
        int maxDigitLength = String.valueOf(yMax*xMax).length();
        for (int i=1;i<=xMax;i++){
            for (int j=1;j<=yMax;j++){

                String tempString = String.valueOf(i * j);
                int length = String.valueOf(i*j).length();
                while (length < maxDigitLength){
                    length ++;
                    tempString += " ";
                }
                returnString+=tempString + " ";
            }
            returnString+="\n";
        }
        return returnString;
    }
}
