/**
 * CircleTester tests a circle class
 * @author:Raymond Tran
 * @date:9/04/18
 */
public class CircleTester{
    public static void main(String[] args){
        Circle myCircle = new Circle(5,5,10);

        System.out.println("Circle Area: " + myCircle.getArea());
        System.out.println("Circle Circumference: " + myCircle.getPerimeter());
        System.out.println("Circle toString: " + myCircle.toString());
    }
}
