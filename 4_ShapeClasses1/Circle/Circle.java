/**
 * Circle creates a circle w/x y position defined at the center
 * Fields: x, y, r
 * Methods: getArea, getPerimeter, toString
 * @author: Raymond Tran
 * @date: 9/04/18
 */
public class Circle{
    private double x;
    private double y;
    private double r;

    public Circle(double x, double y, double r){
        if (r < 0) throw new Error("Radius cannot be negative");
        this.x = x;
        this.y = y;
        this.r = r;
    }

    public double getArea(){
        return Math.PI * Math.pow(r,2);
    }

    public double getPerimeter(){
        return 2 * Math.PI * r;
    }
    public String toString(){
        return "Circle: x: " + x + " y: " + y + " radius: " + r;
    }

    public double getX(){return x;}
    public double getY(){return y;}
    public double getR(){return r;}
    public void setX(double X){x = X;}
    public void setY(double Y){y = Y;}
    public void setR(double R){
        if (R>0) r = R;
        else throw new Error("Radius cannot be negative");
    }


}
