/**
 * CalcTester is used to test the Calc class
 * @author: Raymond Tran
 * @version: 1.0
 * @date: 9/04/18
 */
public class CalcTester{
    public static void main(String[] args){
        double[] testArr = {3.32,7.12};
        Calc[] calculators = {
            new Calc(3.4,7.59),
            new Calc(testArr),
            //Testing
            //new Calc(-7,3)
            //new Calc1(-3,7),
            //new Calc1(3,-7),
            //new Calc1(-7,3),
            //new Calc1(7,-3),
            //new Calc1(-3,-7),
            //new Calc1(-7,-3)



        };
        for (int i=0;i<calculators.length;i++){
            testCalc(calculators[i]);
        }


    }
    private static void testCalc(Calc myCalc){
        double a = myCalc.getA();
        double b = myCalc.getB();
        System.out.println("Testing calc with #'s:" + a + " , " + b);
        System.out.println(a + " plus " + b + " is " + myCalc.getAplusB());
        System.out.println(a + " times " + b + " is " + myCalc.getAtimesB());
        System.out.println(a + " minus " + b + " is " + myCalc.getAminusB());
        System.out.println(b + " minus " + a + " is " + myCalc.getBminusA());
        System.out.println(a + " divided by " + b + " is " + myCalc.getAdivB());
        System.out.println(b + " divided by " + a + " is " + myCalc.getBdivA());
    }
}
